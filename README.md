# Braitenberg vehicles
## INTRODUCTION

Braitenberg vehicles are simple vehicles that show how the interaction between simple rules can lead to complex behaviour. Although these vehicles have no ability to make decisions or even to remember the past, a Braitenberg vehicle may show flexible behaviour and appear to be goal-directed.

![](misc/robo.gif)

For insects such as ants, the obstacle avoidance behavior is one
of the self-adaptive capabilities in their living environments. This behavior involves sensory receptors, neural network, and effectors. Receptors (e.g., eyes of ants) act as afferent neurons and respond to environmental stimuli. The sensed signals from receptors are transmitted through nerve afferent fibers to the special brain compartment where neurons are interconnected in a complex way.

![](misc/ant.png)

## ROBOT AND SENSORS

### [Robot](https://cyberbotics.com/doc/guide/epuck)



E-puck is a miniature mobile robot originally developed at EPFL for teaching purposes by the designers of the successful Khepera robot. The hardware and software of e-puck is fully open source, providing low level access to every electronic device and offering unlimited extension possibilities.

![](misc/robot.jpeg)

The model includes support for the differential wheel motors (encoders are also simulated, as position sensors), the infra-red sensors for proximity and light measurements, the Accelerometer, the Gyro, the Camera, the 8 surrounding LEDs, the body and front LEDs, bluetooth communication (modeled using Emitter / Receiver devices) and ground sensors extension.


|Feature |	Description|
|--------------|-----------|
|Size 	|7.4 cm in diameter, 4.5 cm high|
|Weight 	|150 g
|Battery 	|about 3 hours with the provided 5Wh LiION rechargeable battery
|Processor 	|Microchip dsPIC 30F6014A @ 60MHz (about 15 MIPS)
|Motors 	|2 stepper motors with 20 steps per revolution and a 50:1 reduction gear
|IR sensors 	|8 infra-red sensors measuring ambient light and proximity of obstacles in a 4 cm range
|Camera 	|color camera with a maximum resolution of 640x480 (typical use: 52x39 or 640x1)
|Microphones 	|3 omni-directional microphones for sound localization
|Accelerometer 	|3D accelerometer along the X, Y and Z axes
|Gyroscope 	|3D gyroscope along the X, Y and Z axes
|LEDs 	|8 red LEDs on the ring and one green LED on the body
|Speaker 	|on-board speaker capable of playing WAV or tone sounds
|Switch 	|16 position rotating switch
|Bluetooth 	|Bluetooth for robot-computer and robot-robot wireless communication
|Remote Control 	|infra-red LED for receiving standard remote control commands

### Sensors

|Device 	|Name|
|--------------|-----------|
|Motors 	|'left wheel motor' and 'right wheel motor'
|Position sensors 	|'left wheel sensor' and 'right wheel sensor'
|Proximity sensors 	|'ps0' to 'ps7'
|Light sensors 	|'ls0' to 'ls7'
|LEDs 	|'led0' to 'led7' (e-puck ring), 'led8' (body) and 'led9' (front)
|Camera 	|'camera'
|Accelerometer 	|'accelerometer'
|Gyro 	|'gyro'
|Ground sensors (extension) 	|'gs0', 'gs1' and 'gs2'
|Speaker 	|'speaker'



The forward direction of the e-puck is given by the positive x-axis of the world coordinates. This is also the direction in which the camera eye and the direction vector of the camera are looking. The axle's direction is given by the positive y-axis. Proximity sensors, light sensors and LEDs are numbered clockwise. Their location and orientation are shown in this figure. The last column of the latter lists the angles between the negative y-axis and the direction of the devices, the plane xOy being oriented counter-clockwise. Note that the proximity sensors and the light sensors are actually the same devices of the real robot used in a different mode, so their direction coincides.


![](misc/sensors_and_leds.png)

## ALGORITHM

### Vehicles

Now we will discuss the braitenberg vehicles. Braitenberg vehicles are simple autonomous agents that move around based on sensor input. Thos vehicles have two wheels, each controlled by its own motor. Sensors that measure the light intensity can affect the output of these motors, either positively or negatively. Depending on how where the sensors are located and how these sensors are connected to the motors, the behaviour displayed by the vehicle can differ greatly.

![](misc/types.jpg)

#### Fear
The first vehicle, called Fear, connects the leftmost sensor to the left wheel motor and the rightmost sensor to the right wheel motor. In both cases, the forward speed of the motor is increased when the sensor detects more light. This means that when this vehicle detects light to the left, the left motor increases speed and the vehicle veers to the right. That is, Fear tries to move away from the light.

![](misc/bb_fear.png)

#### Aggression
The second vehicle, Aggression, is similar to Fear, except that the sensors are wired to motor at the opposite end of the vehicle. That is, the leftmost sensor is connected to the right wheel motor while the rightmost sensor connects to the left wheel motor. Like Fear, the sensor connections of Aggression are positive. This means that if the vehicle detects light to the left, the right motor increases speed and the vehicle veers to the left. That is, Aggression moves towards the light. The closer the vehicle gets to the light, the stronger the increase in speed of the motors, until the vehicle speeds through the light.

![](misc/bb_aggression.png)

#### Love
Love is a vehicle that is similar to Fear, except that the sensors decrease the forward speed of the motor to which they are connected. This means that when the vehicle detects light to the left, the left motor decreases speed or even reverses, and the vehicle moves to the left. Like Aggression, Love moves towards the light. Unlike Aggression, however, the closer Love gets to the light, the slower it moves. As a result, Love moves towards the light until it reaches the perfect distance.

![](misc/bb_love.png)

#### Exploration
The final example is Exploration. Exploration has the same crossed wiring of Aggression, except that the sensors decrease the speed of the motors to which they are attached. This means that if the vehicle detects light to the left, the right motor decreases speed or even reverses, and the vehicle veers to the right. Like Fear, Exploration avoids the light. However, Exploration slows down in light areas, almost as if it is cautiously exploring the light.

![](misc/bb_exploration.png)

## WORLD
![](misc/world.png)